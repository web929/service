<?php
#命名空间
namespace Baiyuwan\Wenkai;

trait JsonService
{
    /*
     * 错误码
     * -1 普遍错误
     * */
    public function ajaxSuccess($msg,$data=null){
        if(is_array($msg)){
            $data = $msg;
            $msg = null;
        }
    	exit(json_encode([
    		'code'=>200,
    		'data'=>$data,
    		'message'=>$msg
    	]));
    }

    public function ajaxError($msg,$data=null,$code=-1){
        if(is_array($msg)){
            $data = $msg;
            $msg = null;
        }
    	exit(json_encode([
    		'code'=>$code,
    		'data'=>$data,
    		'message'=>$msg
    	]));
    }

    public function returnSuccess($msg,$data=null){
        if(is_array($msg)){
            $data = $msg;
            $msg = null;
        }
        return json_encode([
            'code'=>200,
            'data'=>$data,
            'message'=>$msg
        ]);
    }

    public function returnError($msg,$data=null,$code=-1){
        if(is_array($msg)){
            $data = $msg;
            $msg = null;
        }
        return json_encode([
            'code'=>$code,
            'data'=>$data,
            'message'=>$msg
        ]);
    }
    public function ajaxReturn($data,$type = 'json'){
        exit(json_encode($data,JSON_UNESCAPED_UNICODE));
    }
    /**
     * 操作成功跳转的快捷方法
     * @access protected
     * @param  mixed $msg 提示信息
     * @param  string $url 跳转的URL地址
     * @param  mixed $data 返回的数据
     * @param  integer $wait 跳转等待时间
     * @param  array $header 发送的Header信息
     * @return void
     */
    public function success($msg = '', string $url = null, $data = null, int $wait = 3, array $header = []){
        if (is_null($url) && isset($_SERVER["HTTP_REFERER"])) {
            $url = $_SERVER["HTTP_REFERER"];
        } elseif ($url) {
            $url = (strpos($url, '://') || 0 === strpos($url, '/')) ? $url : url($url);
        }

        $result = [
            'flag' => true,
            'message' => $msg,
            'data' => $data,
            'url' => $url,
            'wait' => $wait,
        ];

        $type = $this->getResponseType();

        if ('html' == strtolower($type)) {
            $view =  'custom::dispatch_jump';
            return response()->view($view,compact('result'))->withHeaders($header)->send();
        } else {
            return response()->json($result)->withHeaders($header)->send();
        }
    }

    public function error($msg = '', string $url = null, $data = null, int $wait = 3, array $header = []){
        if (is_null($url) && isset($_SERVER["HTTP_REFERER"])) {
            $url = $_SERVER["HTTP_REFERER"];
        } elseif ($url) {
            $url = (strpos($url, '://') || 0 === strpos($url, '/')) ? $url : url($url);
        }

        $result = [
            'flag' => false,
            'message' => $msg,
            'data' => $data,
            'url' => $url,
            'wait' => $wait,
        ];

        $type = $this->getResponseType();

        if ('html' == strtolower($type)) {
            $view =  'custom::dispatch_jump';
            return response()->view($view,compact('result'))->withHeaders($header)->send();
        } else {
            return response()->json($result)->withHeaders($header)->send();
        }
    }

    public function getResponseType(){
        return Request()->ajax() ? 'json' : 'html';
    }
}
?>
