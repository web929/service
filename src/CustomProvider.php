<?php
namespace Baiyuwan\Wenkai;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class CustomProvider extends ServiceProvider
{

    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/resources/views', 'custom');
        //数据迁移
    //    $this->loadMigrationsFrom(__DIR__.'/database/migrations');
    }


}
